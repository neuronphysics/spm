#copy data from here: ftp://ftp.stsci.edu/cdbs/grid/pickles/dat_uvi/
from ftplib import FTP
ftp = FTP('ftp.stsci.edu')                           
ftp.login()         
ftp.cwd('cdbs')     
ftp.cwd('grid')   
ftp.cwd('pickles')   
ftp.cwd('dat_uvi')   
ftp.retrlines('LIST')
for i in range(1,132):
    filename='pickles_'+str(i)+'.fits'
    print filename
    ftp.retrbinary('RETR %s'%filename,  open(filename, 'wb').write)

ftp.quit()