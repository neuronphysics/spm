This is an instruction of using and reproducing results of the **Photometric Calibration of the COMBO-17 Survey with the Softassign Procrustes Matching Method** paper. 
In this repository, [pysysp](https://github.com/mdusilva/pysysp) library has been used in order to convolve Stellar Pickles spectra with *COMBO-17* filter set and get magnitudes in the **Vega System**.

Here you can reproduce the results given in the paper by running the following codes:

 > Get the data from fits file and store it in an ascii file....

**GetCols_Wolf_2008.py**

 > Now you can recalibrate the photometry of CDFS catalogue using two sets of **standard main-sequence stars**...
-Get photometry catalogue of *Pickles synthetic spectra of stars* first by running 

**Making_Pickles_catalogue_of_Main_Sequence_vega_system.py**

-Here we use *SDSS stellar spectra* similar to Kelly et al. 2014 (svn checkout http://big-macs-calibrate.googlecode.com/svn/ big-macs-calibrate)

**run_Softassign_Procrustes_Matching.py**

 > Next step, you can measure photometric redshifts of objects in the catalogue using the BPZ code developed by Benitez 2000..

**Run_BPZ_SDSS_Vega_System.py**


