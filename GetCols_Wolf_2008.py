#Reading data from a file 

import astropy.units as u
import numpy as np
from astropy.io import fits
import re, os
import pysysp
import pyfits, glob,shutil
from astropy.coordinates import SkyCoord

class extract_fits(object):
    """This is the first script that must be run in order to extract stellar 
    uncalibrated catalogue from Wolf et al. 2008 public photometry catalogue"""
    def __init__(self, input_files, filters_folder):
       current_path  = os.getcwd()
       self.vega_file= current_path + '/source/alpha_lyr_stis_006.fits'
       self.inputs   = current_path + input_files
       self.path     = current_path + filters_folder
       self.mainpath = current_path
       os.chdir(self.path)
       self.instrument={}
       #compute the area under each filter
       self.area_filter={}
       for files in glob.glob("WFI_*.res"):
           self.instrument[files] = pysysp.BandPass(self.path + files, smt='linear')
           wr   = self.instrument[files].wavelength
           fr   = self.instrument[files].response
           names= self.instrument[files].name
           self.area_filter[names]=np.trapz(fr * wr, x=wr)
       os.chdir(current_path)
       hdulist = fits.open(self.inputs)
       print hdulist.info()
       self.data    = hdulist[1].data
       self.columns = hdulist[1].data.names
       #photon fluxes of Vega in all filters (10^8 photons/m^2/mn/s)
       #Table 1 column 6 in Wolf et al. 2004 
       self.flux_vega=[ 0.737, 1.371, 1.055, 0.725, 0.412, 1.571, 1.412, 1.207, 1.125, 0.932,\
	                0.832, 0.703, 0.621, 0.525, 0.442, 0.386, 0.380]
       #separate galaxies in the catalogue
       coords = SkyCoord(ra=(hdulist[1].data['RAJ2000']),dec=(hdulist[1].data['DEJ2000']),unit=(u.hourangle,u.deg))
       print "ra & dec:"
       print coords.ra.deg, coords.dec.deg
       hdulist[1].data['RAJ2000']=coords.ra.deg
       hdulist[1].data['DEJ2000']=coords.dec.deg
       GalaxyIndex=(hdulist[1].data['MCclass']=="Galaxy")
       self.Gal = hdulist[1].data[GalaxyIndex]
       print len(self.Gal)
       #separate stars in the catalogue
       StarIndex=(hdulist[1].data['MCclass']=="Star")
       self.Star=hdulist[1].data[StarIndex]
       print len(self.Star)
       #Get the columns related to flux
       self.MB=[]; mbands=[]
       self.BB=[]; bbands=[]
       for value in self.columns:
           m = re.match('W(.+?)F', value)    
           if m!=None:
              self.MB.append(m.group(1))
              mbands.append(value)
           n = re.match('(.?)F', value)
           if n!=None:
              self.BB.append(n.group(1))
              bbands.append(value)
       self.filters=self.BB + self.MB
       #This list contains the name of columns in the original catalogue
       self.mag_columns=bbands+mbands  
       
    def compute_vega_magnitude(self, catalogue):
        """
        The unit of the flux is photons per unit area
        mag = -2.5 * log10(f/f0)
        error(mag) = 2.5 / ln(10) * error(counts) / counts
        """        
        f=self.area_filter.keys()
        self.mag={} ; self.mag_error={}      
        for i, value in enumerate(self.mag_columns):
            for key in f:
                if ( i>=len(self.BB)):
                   if (self.filters[i][:-1] in key[3:]):
                      flux_ratio=catalogue[value]/(self.flux_vega[i]*1e8)
                      self.mag[value]=-2.5*np.log10(flux_ratio)
                      self.mag_error['e_'+value]=(2.5/np.log(10))*(catalogue['e_'+value]/catalogue[value])
                else:
                   if (self.filters[i] in key[3:]):
                      flux_ratio=catalogue[value]/(self.flux_vega[i]*1e8)
                      self.mag[value]=-2.5*np.log10(flux_ratio)
                      self.mag_error['e_'+value]=(2.5/np.log(10))*(catalogue['e_'+value]/catalogue[value])
            mask=np.where(catalogue[value]<=0.0)[0]
            self.mag[value][mask]=-99.0
            self.mag_error['e_'+value][mask]=0.0
            test=np.vstack((catalogue[value], self.mag[value], self.mag_error['e_'+value])).T
            #print the computed magnitudes and errors from flux values
            #np.savetxt('flux_magnitude_'+self.filters[i]+'.asc',test)                
        #Generate a dictionary to store data with headers 
        Columns_List=['Seq', 'RAJ2000', 'DEJ2000', 'Xpos', 'Ypos', 'Rmag', 'e_Rmag', 'ApRmag', \
	              'ApDRmag', 'MCclass', 'MCz', 'e_MCz', 'MCz2', 'e_MCz2', 'MCzml',\
                      'UFF', 'BFD', 'VFD', 'RFD', 'IFD', 'W420FE', 'W462FE', 'W485FD', 'W518FE',\
                      'W571FD', 'W604FE', 'W646FD', 'W696FE', 'W753FE','W815FE', \
                      'W856FD', 'W914FD', 'e_UFF', 'e_BFD', 'e_VFD','e_RFD', 'e_IFD',\
                      'e_W420FE', 'e_W462FE', 'e_W485FD', 'e_W518FE', 'e_W571FD', 'e_W604FE',\
                      'e_W646FD', 'e_W696FE', 'e_W753FE', 'e_W815FE', 'e_W856FD', 'e_W914FD']
        mydictionary={key: [] for key in Columns_List}
        print mydictionary,len(mydictionary)
        for i in range(len(catalogue[Columns_List[0]])):
            for counter, key in enumerate(Columns_List):
	        if ((key not in self.mag.keys()) and (key not in self.mag_error.keys())):
                   #print key
                   mydictionary[key].append(str(catalogue[key][i])) #get the values of these columns from original catalogue
                elif (key in self.mag.keys()):
                   #print "mag value ", key
                   mydictionary[key].append(str(self.mag[key][i]))
                else:
                   #print "mag error value", key
                   mydictionary[key].append(str(self.mag_error[key][i]))
        outfilename=self.mainpath+'/data/CDFS_'+catalogue['MCclass'][0]+'.asc'
        f =  open(outfilename,'w')        
        for j in range(len(mydictionary[Columns_List[0]])):
            for i in range( 0, Columns_List.__len__()):
                f.write("%s\t" % mydictionary[Columns_List[i]][j])
            f.write('\n')            
        f.close()
        #Adding header to the output file
        tmp = open('tmp', 'w')
        orig = open(outfilename, 'r')
        tmp.write('#'+'\t'.join(Columns_List) + '\n')
        for line in orig.readlines():
             tmp.write(line) 
    
        orig.close()
        tmp.close()
        try:
            shutil.move('tmp', outfilename)
        except OSError, e:  ## if failed, report it back to the user ##
            print ("Error: %s - %s." % (e.outfilename,e.strerror))

d=extract_fits('/source/asu.fit','/filters/')
d.compute_vega_magnitude(d.Star)
d.compute_vega_magnitude(d.Gal)
