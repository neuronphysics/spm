#!/usr/bin/env python
# -*- coding: utf-8 -*-
import math
import numpy as np
import scipy
import scipy.linalg
import pylab as P
import matplotlib
import sys
import os
import pandas
import scipy.stats
import seaborn
import scipy.spatial.distance
"""
This function can be used to discard scatter points from stellar locus using KDE method
note: This is a preparation step before colour calibration. 
"""
def refineStellarLocusKDE(Colorx,Colory, ratio, **kwargs):
    """
    Remove objects whose density is below 1/ratio of the mean density.
    The point density is estimated by Gaussian kernel convolution,
    with automatic bandwidth determination.
    Inputs: the color pair Colorx and Colory (the x- and y-color)
            the threshold ratio for the cut
            if Plot=True and plot_dir is given then the function plots the data
    optional inputs:
           make_plot: for cross check the performence of kde should be a boolean 'True' or 'False'
           plot_value: A string for the path to the plots if make_plot=True
    Output: the object index array of excluded points
    """
    for j in range(Colorx.shape[1]):
        X=np.array([[Colorx[i,j],Colory[i,j]] for i in range(Colorx.shape[0])])
        data = pandas.DataFrame(X, columns=["X", "Y"])
        kernel = scipy.stats.gaussian_kde(X.T)
        kernel.set_bandwidth(bw_method='scott')
        KdeEval=np.zeros(Colorx.shape[0],float)
        for i in range(Colorx.shape[0]):
            KdeEval[i]=kernel.evaluate(X[i,:])
        ex=np.where(KdeEval<KdeEval.mean()/ratio)
        if (j==0):
           exarr=ex[0]
        else:
           if len(ex[0])!=0:
              exarr=np.unique(np.concatenate((exarr,ex[0])))
    if 'make_plot' in kwargs:
       plot_value=kwargs.pop('make_plot')
       if plot_value:
	 try:
	    dir_value=kwargs.pop('plot_dir')
         except KeyError:
            raise TypeError("Must use keyword argument 'plot_dir' if 'make_plot' is True")
         except ValueError:
            raise ValueError("'plot_dir' must be an string")
       try:
         # Are there anymore keyword arguments? We don't care which one we get
         x = next(iter(kwargs))
       except StopIteration:
         pass
       else:
         raise TypeError("Unrecognized keyword argument '{0}'".format(x))
	  
       for j in range(Colorx.shape[1]):
           X=np.array([[Colorx[i,j],Colory[i,j]] for i in range(Colorx.shape[0])])
           data = pandas.DataFrame(X, columns=["X", "Y"])
           seaborn.kdeplot(data.X,data.Y,bw='scott',shade=True, cmap="Purples")
           P.scatter(Colorx[:,j],Colory[:,j],marker='.',s=2,color='red')
           P.scatter(Colorx[exarr,j],Colory[exarr,j],s=13,facecolors='none', edgecolors='k')
           figname = '%s/refineStellarLocus.kde.%d.eps' % (dir_value,j, )
           print figname
           P.savefig(figname)
           P.close()
    return exarr

"""
Softassign Procrustes Annealing (SPA) module
   For color calibration: take two point-sets and find correspondence, translation, rotation, scaling

"""

#
# Functions
#
def mu(x,m):
    """function for calculating the weighted centroid of point-set.
       x is the 2D point-set and M is the matrix of correspondence (the weight).
    """
    if (m.shape[0]==x.shape[0]):
        return np.array([np.sum(np.dot((x[:,0]).T,m))/np.sum(m),
                         np.sum(np.dot((x[:,1]).T,m))/np.sum(m)])
    else:
        return np.array([np.sum(np.dot(m,x[:,0]))/np.sum(m),
                         np.sum(np.dot(m,x[:,1]))/np.sum(m)])

def sigma(x,m):
    """function for calculating the weighted variance of data point relative to mu.
       x is the 2D point set and m is the correspondence matrix (the weight).
    """ 
    z=x-mu(x,m)
    s=np.zeros(2,float)  
    if (m.shape[0]==x.shape[0]):
       for i in range(m.shape[0]):
           for j in range(m.shape[1]):
               s[0]+=m[i,j]*((z[i,0])**2)
               s[1]+=m[i,j]*((z[i,1])**2)
    else:
       for i in range(m.shape[0]):
           for j in range(m.shape[1]):
               s[0]+=m[i,j]*((z[j,0])**2)
               s[1]+=m[i,j]*((z[j,1])**2)
    return s/np.sum(m)

def procrustes(x,m,rescale=True):
    """moves each point realtive to the centroid mu, and rescales distance normalized to sigma.
    The rescaled x are the set of the Procrustes points for computing Procurstes distances between
    point-sets.
    x is the point set and m is the correspondence matrix"""
    if rescale:
        return (x-mu(x,m)) / np.sqrt(sigma(x,m))
    else:
        return (x-mu(x,m))

def theta(x,y,m,rescale=True):
    """Theta is the angle that the Procurstes distance points set should rotate in order to
    coincide with the other Procurstes point set.
    x the abscissa component of the data set and y the ordinate one and m refers to 
    correspondence matrix"""
    n=0
    d=0
    rx=procrustes(x,m,rescale)
    ry=procrustes(y,m,rescale)
    for i in range(m.shape[0]):
        for j in range(m.shape[1]):
            n+=m[i,j]*(rx[i,1]*ry[j,0]-rx[i,0]*ry[j,1])
            d+=m[i,j]*(rx[i,0]*ry[j,0]+rx[i,1]*ry[j,1])
    t=np.arctan2(n,d)
    #print "new angle:",(t*180)/pi
    return t

def R(t):  
    """The rotation matrix"""
    z=np.array([[np.cos(t), -np.sin(t)], [np.sin(t), np.cos(t)]])
    return z

def translation(x,y,m):
    """The function for computing the translation between two data sets"""
    t=np.zeros(2,float)
    for i in range(x.shape[0]):
        for j in range(y.shape[0]):
            t+=(m[i,j]*(x[i,:]-y[j,:]))/np.sum(m)
    return t

def Std(x,y,m,t):
    s=np.zeros(2,float)
    for i in range(m.shape[0]):
        for j in range(m.shape[1]):
            s+=(m[i,j]*(x[i,:]-y[j,:]-t)**2)/np.sum(m)
    return s 

def max_dist(x):
    """returns the maximum distance available for all possible point pair in x
    """
    return np.array(max(x[:,0])-min(x[:,0]), max(x[:,1])-min(x[:,1]))


def maximum_dist(x):
    r=scipy.spatial.distance.pdist(x)
    return np.max(r)
#
# define SPA class
#
class SoftassignProcrustesAnnealing(object):

    def __init__(self, data_xy, ref_xy, rescale=False, rotate=False, label='', data_dir='data'):

        ## the main data
        self.dd = data_xy   # self.dd.shape[0] == num_obj
        self.mod = ref_xy   # self.mod.shape[0] == num_ref_obj

        ## where intermediate files (such as the correspondence matrix) are saved
        self.data_dir = data_dir    # the code assumes that data_dir exists

        ## the labels for the intermediate files
        self.label = label

        ## Do the shapes require rotation or rescaling?
        #  Should be set to False.  We find that allowing degrees of freedom that do not exist
        #  in the data and reference point comparison makes the Softassign process unstable.
        #  (The instability stems from the difference in the number of points of data and ref
        #  and/or the lack of features in the pointsets.)
        self.rescale = rescale
        self.rotate = rotate

        ## slack parameter ALPHA: controls the number of rejected points
        self.alpha = 0.001
        #self.alpha = 0.0005  # The alpha term biases the objective towards matches. It acts as
                            # a threshold error distance, indicating how far apart two points must be
                            # in order to have it considered an "exact match" (i.e., dist = 0)
        ## annealing parameter BETA
        self.beta_i = 100   # initial value of the inverse temperature parameter
                            # a rule of thumb: 10. * np.sqrt(N1*N2)
        self.beta_r = 1.075 # rate at which the inverse temperature parameter is increased
        self.max_beta = 0.1e6 # maximum beta for the deterministic annealing.
                            # a rule of thumb: 1000 * np.sqrt(N1*N2)

        ## Softassign iteration parameters
        self.I0 = 1   # iteration for M convergence
        self.I1 = 30  # iteration for Mhat convergence

        ## threshold values (determine with "print k, np.linalg.norm(self.Mhat - prev_Mhat)")
        self.Mhat_norm_thres = 1e-5  # criteria for Mhat convergence (linear)
        self.M_norm_thres = 1e-5
        self.relative=False
        return


    def getCorrespondenceMatrix(self):
        """
        Return correspondence matrix given the base correspondence matrix Mhat
        """

        if not hasattr(self, 'Mhat'):  # does self.Mhat not exist?

            ## first, check for existing file, see if we can load in self.Mhat
            self.Mhat_fname = os.path.join(self.data_dir, 'SPM.HomologyMatrix_%s.asc' % (self.label))

            try:
                self.Mhat = np.loadtxt(self.Mhat_fname)
            except IOError:
                self.calculateMhat()
                
        #
        # remove the last extra row and column from Mhat to get the correspondence matrix M
        #
        M = np.vsplit(self.Mhat, (self.dd.shape[0],self.dd.shape[0]+1))[0]
        M = np.hsplit(M, (self.mod.shape[0],self.mod.shape[0]+1))[0]
        return M


    def calculateMhat(self):
        """
        Calculate the base correspondence matrix Mhat, using Softassign Procrustes Annealing
        """
        ## initialize correspondence matrix "M" with extra rows and columns
        epsilon = 0.005
        self.Mhat = np.ones((self.dd.shape[0]+1, self.mod.shape[0]+1),)  # with extra row and col
        self.Mhat *= (1+epsilon)
        self.Q = np.zeros_like(self.Mhat)

        ## initialize rotation angle and translations
        if (len(self.dd.shape)>2):
           self.angle =   np.zeros(self.dd.shape[2],float)
        else:   
           self.angle = 0.0              # rotation angle (in degrees)
        self.scaling = 1.0            # relative scaling
        self.tf = np.zeros(2)         # 2D translation

        ## determine the inverse temperature beta0 for outlier objects.  (beta0 does not evolve!)
        if (len(self.dd.shape)>2):
	   dd_max=[];mod_max=[]
	   for i in range(self.dd.shape[2]):
	       if self.rescale:     
	           dd_max.append(maximum_dist(self.dd[:,:,i]/sigma(self.dd[:,:,i],self.getM())))  
		   mod_max.append(maximum_dist(self.mod[:,:,i]/sigma(self.mod[:,:,i],self.getM())))
	       else:	     
		   dd_max.append(maximum_dist(self.dd[:,:,i]))  
		   mod_max.append(maximum_dist(self.mod[:,:,i]))
	   dd_sq = max(dd_max)
	   mod_sq =max(mod_max)
	else:
           if self.rescale:
              dd_sq = maximum_dist(self.dd/sigma(self.dd,self.getM()))
              mod_sq = maximum_dist(self.mod/sigma(self.mod,self.getM()))
           else:
              dd_sq = maximum_dist(self.dd)
              mod_sq = maximum_dist(self.mod)
        self.beta0 = 0.5 * max(dd_sq, mod_sq)**(-2)

        #
        # Deterministic Annealing loop
        #
        some_large_number = 1e5
        beta = self.beta_i  # initialize control parameter of the deterministic annealing method
        print "initial quantity of beta:\n", beta  # DEBUG
        while (beta < self.max_beta):

            prev_M = self.getM()
            for l in range(self.I0):
                if (len(self.dd.shape)>2):
                   ## update rotation angle
                   self.model = []
                   self.data  = []
                   for k in range(self.dd.shape[2]):
                       if self.rotate:
                          self.angle[k] = theta(self.dd[:,:,k], self.mod[:,:,k], self.getM()) * 180. / np.pi

                       ## calculate Procrustes "points" with updated centroids, variances, rotation
                       self.model.append( np.transpose(np.dot(R(self.angle[k]), 
                                                    np.transpose(procrustes(self.mod[:,:,k],self.getM(),
                                                                         rescale=self.rescale)))))
                       self.data.append( procrustes(self.dd[:,:,k], self.getM(), rescale=self.rescale))

                   ## begin softassign
                   
                   for i in range(self.dd.shape[0]+1):
                       for j in range(self.mod.shape[0]+1):
                           aggregate=0
                           ## pseudo-Procrustes distance "diff"
                           if i != self.dd.shape[0]:
                              if j != self.mod.shape[0]:
				 for k in range(self.dd.shape[2]):
                                     diff=self.data[k][i,:]-self.model[k][j,:]
                                     aggregate+=np.dot(diff, np.transpose(diff))/float(self.dd.shape[2])
                              else:
				 for k in range(self.dd.shape[2]):
                                     diff=self.data[k][i,:]  # outlier Procrustes distance for i
                                     aggregate+=np.dot(diff, np.transpose(diff))/float(self.dd.shape[2])
                           elif j != self.mod.shape[0]:
			        for k in range(self.dd.shape[2]):
                                    diff=self.model[k][j,:]     # outlier Procrustes distance for j
                                    aggregate+=np.dot(diff, np.transpose(diff))/float(self.dd.shape[2])
                           else:   # i == self.dd.shape[0] and j == self.mod.shape[0]
                                aggregate= some_large_number
                           ## diff is small for Procrustes points that match well (but always >0)
                           self.Q[i,j] = max(aggregate - self.alpha, 0.0)
                           ## reward (i,j) pairs with small diff
                           if i == self.dd.shape[0] and j == self.mod.shape[0]:   # must set to 0
                               self.Mhat[i,j] = 0.0
                           elif ((i == self.dd.shape[0] or j == self.mod.shape[0]) and not (i == self.dd.shape[0] and j == self.mod.shape[0])):  # outliers have
                               self.Mhat[i,j] = np.exp(-self.beta0 * self.Q[i,j]) # constant temp.
                           else:
                               self.Mhat[i,j] = np.exp(-beta * self.Q[i,j])
                else:
                   ## update rotation angle
                   if self.rotate:
                       self.angle = theta(self.dd, self.mod, self.getM()) * 180. / np.pi

                   ## calculate Procrustes "points" with updated centroids, variances, rotation
                   self.model = np.transpose(np.dot(R(self.angle), 
                                                 np.transpose(procrustes(self.mod,self.getM(),
                                                                         rescale=self.rescale))))
                   self.data = procrustes(self.dd, self.getM(), rescale=self.rescale)

                   ## begin softassign
                   for i in range(self.dd.shape[0]+1):
                       for j in range(self.mod.shape[0]+1):
   
                           ## pseudo-Procrustes distance "diff"
                           if i != self.dd.shape[0]:
                               if j != self.mod.shape[0]:
                                   diff = self.data[i,:]-self.model[j,:]
                               else:
                                   diff = self.data[i,:]  # outlier Procrustes distance for i
                           elif j != self.mod.shape[0]:
                               diff = self.model[j,:]     # outlier Procrustes distance for j
                           else:   # i == self.dd.shape[0] and j == self.mod.shape[0]
                               diff = some_large_number
   
                           ## diff is small for Procrustes points that match well (but always >0)
                           self.Q[i,j] = max(np.dot(diff, np.transpose(diff)) - self.alpha, 0.0)
                           ## reward (i,j) pairs with small diff
                           if i == self.dd.shape[0] and j == self.mod.shape[0]:   # must set to 0
                               self.Mhat[i,j] = 0.0
                           elif ((i == self.dd.shape[0] or j == self.mod.shape[0]) and not (i == self.dd.shape[0] and j == self.mod.shape[0])):  # outliers have
                               self.Mhat[i,j] = np.exp(-self.beta0 * self.Q[i,j]) # constant temp.
                           else:
                               self.Mhat[i,j] = np.exp(-beta * self.Q[i,j])
                ## normalize sumf of rows and columns of Mhat to 1 via the Sinkhorn algorithm
                for k in range(self.I1):

                    prev_Mhat = np.copy(self.Mhat)

                    sx = np.dot(self.Mhat, np.ones(self.Mhat.shape[1]))  # sum of each row
                    for i in range(sx.shape[0]):
                        self.Mhat[i,:] = self.Mhat[i,:]/sx[i]
                    sy = np.dot(np.ones(self.Mhat.shape[0]), self.Mhat)  # sum of each col
                    for i in range(sy.shape[0]):
                        self.Mhat[:,i] = self.Mhat[:,i]/sy[i]

                    ## check for convergence of Mhat
                    if np.linalg.norm(self.Mhat - prev_Mhat) < self.Mhat_norm_thres:
                        break
                if (len(self.dd.shape)>2):
		   print translation(self.dd[:,:,0], self.mod[:,:,0], self.getM())
		else:
                   print translation(self.dd, self.mod, self.getM()),  # PROGRESS DEBUG, distance
                ## end softassign

                ## check for convergence of M  (self.I0=1 is sufficient!)
                if (self.I0 > 1) and (np.linalg.norm(self.getM()-prev_M) < self.M_norm_thres):
                    break

            ## update beta
            beta = self.beta_r * beta
            print np.sum(self.getM()>0.99),  # PROGRESS DEBUG, number of definitely matched objects
            print int(np.sum(self.getM())),  # PROGRESS DEBUG, number of objects that have a match
            print beta                       # PROGRESS DEBUG, annealing parameter (inv. temp.)

        #
        # translation found
        #
        if (len(self.dd.shape)>2):
	   self.tf = translation(self.dd[:,:,0], self.mod[:,:,0], self.getM())
	else:
	   self.tf = translation(self.dd, self.mod, self.getM())
        print
        print 'M (final)', self.getM()   ## DEBUG
        print 'M dimension', self.getM().shape   ## DEBUG
        print 'sum(M, axis=0)', np.sum(self.getM(), axis=0)   ## DEBUG
        print np.sum(self.getM()), 'points found correspondence'  ## DEBUG
        print "slack row:\n",self.Mhat[-1,:]
        np.savetxt(os.path.join(self.data_dir,'SPM.slack.row.asc'),self.Mhat[-1,:] , fmt='%.3f')
        print "slack column:\n",self.Mhat[:,-1]
        np.savetxt(os.path.join(self.data_dir,'SPM.slack.column.asc'),self.Mhat[:,-1] , fmt='%.3f')
        ## save the Mhat matrix
        np.savetxt(self.Mhat_fname, self.Mhat, fmt='%.3f')

        # Mhat is determined at the end of the deterministic annealing loop (here).
        # To check for convergence and test annealing parameters, print the various values above.


    def getM(self):
        """synonym of getCorrespondenceMatrix()
        """
        return self.getCorrespondenceMatrix()


    def getRotation(self):
        """get relative rotation angle (in degrees) [UNUSED]
        """
        if not self.rotate:
            return 0.0
        else:
            self.getM()  # internally calculates self.angle
            return self.angle


    def getScaling(self):
        """get relative scaling [UNUSED]
        """
        if not self.rescale:
            return 1.0
        else:
            return sigma(self.dd,self.getM()) / sigma(self.mod,self.getM())
        

    def getTranslation(self, data_xy=None, ref_xy=None):
        """
        Return optimal translation (dx, dy), given two arrays "ref" and "data".
        Both "ref" and "data" are an array of 2-dimensional points.
        The translation is such that  data = ref_xy + (dx,dy)
        """
        
        if data_xy == None:
            data_xy = self.dd
        if ref_xy == None:
            ref_xy = self.mod

        if (len(data_xy.shape)>2):
	   t = translation(data_xy[:,:,0], ref_xy[:,:,0], self.getM())
	else:
	   t = translation(data_xy, ref_xy, self.getM())
        print "translation:", t  ## DEBUG

        return t


    def generateCoefficientMatrix(self, color_pair_filter_index, color_data, color_ref, matching_matrix, filter_list, fix_index= None):
        """
        Generate (internally) the color coefficient matrix A, where in the matrix equation
            A dot X = b
        X is the final translation per filter, and b is the (previously obtained) color shift.
        fix_index:the index of the band as reference where for example we fix magnitude zero point of R band
        [new variables: self.coeff, self.b]
        """

        self.num_color_pairs = color_pair_filter_index.shape[0]
        num_obj = color_data.shape[0]
        num_ref_obj = color_ref.shape[0]
        self.num_filters = len(filter_list)

        # coefficient matrix for linear solution of minimum chi-square
        self.coeff = np.zeros((2*self.num_color_pairs, self.num_filters), float)
        self.b = np.zeros(self.coeff.shape[0], float)
        self.corr_matrix=matching_matrix
        ii = 0
        tt = np.zeros((self.num_color_pairs, 2), float)  # this is the translation in x and y
        if fix_index is not None:
           self.relative = True
           for k in range(self.num_color_pairs):

               # from the correspondence matrix, figure out the shifts for the other color-pairs
               cunt=0.0;shift=0.0
               for i in range(num_obj):          # "i" is data
                   for j in range(num_ref_obj):  # "j" is reference
                       if self.corr_matrix[i,j]>0.95:
                          shift+= self.corr_matrix[i,j] * (color_data[i,k,:]-color_ref[j,k,:])
                          cunt+= self.corr_matrix[i,j]    
               tt[k,:]=shift/cunt      
               #print "angle:\n",angle
               print "translation:\t",k,"---",tt[k,:]   # PROGRESS print
               #print k,    # PROGRESS print
               sys.stdout.flush()

               # we have "griz" or "gryz", and we want to fix the r band
               print 'fixing the', filter_list[fix_index], 'band as no zero-point offset'
               self.b[ii] = tt[k,0]
               if (color_pair_filter_index[k,0] == fix_index) :
                  self.coeff[ii,color_pair_filter_index[k,0]] =  0.0
                  self.coeff[ii,color_pair_filter_index[k,1]] = -1.0
               elif (color_pair_filter_index[k,1] == fix_index):
                  self.coeff[ii,color_pair_filter_index[k,0]] =  1.0
                  self.coeff[ii,color_pair_filter_index[k,1]] =  0.0
               else:
                  self.coeff[ii,color_pair_filter_index[k,0]] =  1.0
                  self.coeff[ii,color_pair_filter_index[k,1]] = -1.0
               ii += 1
               self.b[ii] = tt[k,1]
               if (color_pair_filter_index[k,1] == fix_index) :
                  self.coeff[ii,color_pair_filter_index[k,1]] =  0.0
                  self.coeff[ii,color_pair_filter_index[k,2]] = -1.0
               elif (color_pair_filter_index[k,2] == fix_index) :
                  self.coeff[ii,color_pair_filter_index[k,1]] = 1.0
                  self.coeff[ii,color_pair_filter_index[k,2]] = 0.0
               else:
                  self.coeff[ii,color_pair_filter_index[k,1]] =  1.0
                  self.coeff[ii,color_pair_filter_index[k,2]] = -1.0
               ii += 1
        else:
           for k in range(self.num_color_pairs):

                # from the correspondence matrix, figure out the shifts for the other color-pairs
                cunt=0.0;shift=0.0
                for i in range(num_obj):          # "i" is data
                    for j in range(num_ref_obj):  # "j" is reference
                        if self.corr_matrix[i,j]>0.95:
                           shift+= self.corr_matrix[i,j] * (color_data[i,k,:]-color_ref[j,k,:])
                           cunt+= self.corr_matrix[i,j]    
                tt[k,:]=shift/cunt      
                #print "angle:\n",angle
                print "translation:\t",k,"---",tt[k,:]   # PROGRESS print
                #print k,    # PROGRESS print
                sys.stdout.flush()

                # we have "griz" or "gryz", and we want to fix the r band
                #No filter value is fixed
                self.b[ii] = tt[k,0]
                self.coeff[ii,color_pair_filter_index[k,0]] =  1.0
                self.coeff[ii,color_pair_filter_index[k,1]] = -1.0
                ii += 1
                self.b[ii] = tt[k,1]
                self.coeff[ii,color_pair_filter_index[k,1]] =  1.0
                self.coeff[ii,color_pair_filter_index[k,2]] = -1.0
                ii += 1
        print

        print "coefficients:\n", self.coeff
        print
        print "directory:\n", self.data_dir
        print
        print "b:", self.b
        np.savetxt(os.path.join(self.data_dir,'SPM.translate.update.asc'), tt, fmt='%.3f')  ## DEBUG

        return



    def runCoefficientMatrixSVD(self):
        """
        computing the offsets in zero-points using coefficient matrix
        and all translations in all color-color spaces just by using 
        SPM on a few of them
        """
        if not hasattr(self, 'coeff'):  # does self.best_M not exist?
           raise ValueError('you should first run generateCoefficientMatrix .....')
        
        if self.relative:
           #
           # Singular value decomposition  (solve for magnitude shifts)
           #
           U, s, Vh = scipy.linalg.svd(self.coeff, full_matrices=False)
           print "==============================SVD calculation============================"
           print U.shape, Vh.shape, s.shape
           print U
           print Vh
           print s
           print
           S = scipy.linalg.diagsvd(s, self.num_filters, self.num_filters)
           print 'SVD composition equivalent to original?',
           print np.allclose(self.coeff, np.dot(U, np.dot(S, Vh)))
           Sh = scipy.linalg.inv(S)

           for i in range(Sh.shape[0]):
               if Sh[i,i] > 1.0e+04 :
                   Sh[i,i]=0

   
           print "Matrix W and it's inverse:\n",s
           print

           Uh = scipy.transpose(U)
           V = scipy.transpose(Vh)
           aa = np.dot(V, np.dot(Sh, Uh))
           print 'a_SVD:\n', aa

           aah = scipy.transpose(aa)


           S_sq = np.dot(Sh,Sh)
           print
           print S_sq
           print
           V_sq = np.dot(V,Vh)
           covar = np.dot(S_sq,V_sq)

           np.savetxt(os.path.join(self.data_dir,'Singular_aa.asc'), aa, fmt='%.3f')
           np.savetxt(os.path.join(self.data_dir,'Singular_b.asc'), self.b, fmt='%.3f')

           res = np.dot(aa, self.b)   # THIS IS THE SOLUTION

           wt = np.zeros(s.shape[0], float)
           for i in range(s.shape[0]):
               wt[i] = 0
               if math.fabs(s[i]) > 1.0e-04:
                   wt[i] = 1./(s[i]*s[i])

           print "WT:\n", wt
           print "V:\n", Vh
           print
           cvm = np.zeros((s.shape[0],s.shape[0]), float)
           for i in range(s.shape[0]):
               j = 0
               while j <= i:   
                 cum = 0.0 
                 for k in range(s.shape[0]):
                    cum = cum + Vh[i,k] * Vh[j,k] * wt[k]

                 cvm[i,j] = cum
                 cvm[j,i] = cum
                 j += 1

           print "SVD results for", self.num_filters, "filters:\n", res
           print "SVD's covariance matrix:\n", cvm
           print
           print 'b:\n', self.b
           sig = np.zeros(cvm.shape[0], float)
           for i in range(cvm.shape[0]):
               for j in range(cvm.shape[1]):
                   if i==j:
                       sig[i] = np.sqrt(cvm[i,j])
           print
           print 'Variance (you mean sigma):\n', sig
           print

           chi_squared = 0
           v = np.dot(self.coeff, res)
           print v
           for i in range(self.b.shape[0]):
               chi_squared += (self.b[i]-v[i])**2
   

           print "chi_squared:\n", chi_squared
           print
           reduced_chi = chi_squared / (self.coeff.shape[0]-self.coeff.shape[1]-1)
           print "Reduced-Chi-squared:\n", reduced_chi

           return res

        else:
           W=self.coeff
           Y=self.b.reshape((len(self.b), 1))
           from numpy.linalg import pinv
           m, n = Y.shape
           k = self.num_filters
           X = pinv(W.T.dot(W)).dot(W.T).dot(Y)
           Y_hat = W.dot(X)
           Residuals = Y_hat - Y
           MSE = np.square(Residuals).sum(axis=0) / (m - 2)
           X_var = (MSE[:, None] * pinv(W.T.dot(W)).flatten()).reshape(n, k, k)
           print X_var
           Tstat = X / np.sqrt(X_var.diagonal(axis1=1, axis2=2)).T
           print
           print "Diagonal elements of the covariance of errors:"
           print Tstat
           np.savetxt(os.path.join(self.data_dir,'SPM.Diagonal_covariance_error.asc'), Tstat, fmt='%.4f')
           return X
      
      
    def get_offsets(self):
        if not self.relative:
           from scipy.sparse.linalg import lsqr
           offsets= lsqr(self.coeff, self.b, damp=0.1, atol=1e-08, btol=1e-08, conlim=1000000000000.0, iter_lim=500)[0]
           print offsets
           return offsets
        else:
           raise ValueError('This function can only be used if fix_index=None .....')	  


seaborn.set_style(style='white')
class PlotStellarLocusKDE(object):
      def __init__(self, icolorX, icolorY,fcolorX, fcolorY, colorpX, colorpY , dx, dy, excluded_points, lcolorx1, lcolorx2, lcolory1, lcolory2, correspondence_Matrix, plot_label=True):
	
          self.exarr=excluded_points #scatter points excluded by kde
          self.icolorx=icolorX #initial stellar locus without rejection by kde
          self.icolory=icolorY
          self.fcolorx=fcolorX #final stellar locus after rejection of some points by kde
          self.fcolory=fcolorY
          self.colorpx=colorpX #pickles stellar locus color in x direction
          self.colorpy=colorpY
          r=np.arange(self.icolorx.shape[0])
          self.arr=np.setxor1d(r,self.exarr) #obtain the included points
          
          self.lx1=lcolorx1 #first band name used to make color in x direction
          self.lx2=lcolorx2 #second band is used to make color in x direction
          self.ly1=lcolory1 #first band name used to make color in y direction
          self.ly2=lcolory2 #second band is used to make color in y direction
          self.M=correspondence_Matrix
          #just get the indicies where M_{ij} is greater than 0.99
          correspondence_indicies = np.where(self.M > 0.99)
          self.colorx_corr=self.fcolorx[correspondence_indicies[0]]
          self.colory_corr=self.fcolory[correspondence_indicies[0]]
          self.colorpx_corr=self.colorpx[correspondence_indicies[1]]
          self.colorpy_corr=self.colorpy[correspondence_indicies[1]]      
          self.PlotLabels=plot_label #Decide whether I want label in the plot or not
          self.dx=dx #The translation in the x direction computed from SPM method
          self.dy=dy#The translation in the y direction computed from SPM method
      
      def __repr__(self):
          return 'print the stellar locus in %s-%s vs. %s-%s diagram' % (self.lx1 ,self.lx2, self.ly1,self.ly2)   
      
      def plot_before_colors(self):
          fig=P.figure(1, figsize=(8,8), dpi=100)
          ax = fig.add_subplot(111)
          X=np.vstack((self.icolorx, self.icolory)).T
          data = pandas.DataFrame(X, columns=["X", "Y"])
          #seaborn.kdeplot(data.X,data.Y,bw='scott',shade=False, cmap="Purples_d") 
          seaborn.kdeplot(data.X,data.Y,bw='scott',shade=True, cmap="PuRd") 
          ax.tick_params(axis='both', which='major', direction='in', length=6, width=2)
          ax.scatter(self.icolorx[self.exarr], self.icolory[self.exarr], s=65, c='m', marker='o', edgecolors='k',facecolors='none')
          ax.scatter(self.icolorx, self.icolory ,marker='p',s=35, color='b', alpha=0.8,edgecolor='k')
          ax.scatter(self.colorpx, self.colorpy, s=35, c='r', marker='*', edgecolor='r',label='Pickles stars')
          for i in range(len(self.colorx_corr)):
              ax.annotate("",
                          xy=(self.colorpx_corr[i], self.colorpy_corr[i]), xycoords='data',
                          xytext=(self.colorx_corr[i], self.colory_corr[i]), textcoords='data',
                          arrowprops=dict(arrowstyle="->",
                          connectionstyle="arc3"), 
                          color='0.3'
                          )
          ax.set_xlabel("%s - %s"%(self.lx1,self.lx2), size='large')
          ax.set_ylabel("%s - %s"%(self.ly1,self.ly2), size='large')
          ax.set_aspect('auto')
          if self.PlotLabels:
             ax.legend(loc=1, prop={'size':12})
          figname = os.environ['SPM'] + '/colors/StellarLocus.kde.%s-%s.vs.%s-%s.before.color.calibration.png' % (self.lx1,self.lx2,self.ly1,self.ly2 )
          P.savefig(figname)
          P.close()
      
      def plot_after_colors(self):
          fig=P.figure(1, figsize=(8,8), dpi=100)
          ax = fig.add_subplot(111)
          ax.tick_params(axis='both', which='major', direction='in', length=6, width=2)
          ax.scatter(self.icolorx[self.exarr]-self.dx, self.icolory[self.exarr]-self.dy, s=65, c='m', marker='o', edgecolors='k',facecolors='none')
          ax.scatter(self.icolorx-self.dx, self.icolory-self.dy ,marker='p',s=35, color='b', alpha=0.8,edgecolor='k')
          ax.scatter(self.colorpx, self.colorpy, s=35, c='r', marker='*', edgecolor='r',label='Pickles stars')
          ax.set_xlabel("%s - %s"%(self.lx1,self.lx2), size='large')
          ax.set_ylabel("%s - %s"%(self.ly1,self.ly2), size='large')
          ax.set_aspect('auto')
          if self.PlotLabels:
             ax.legend(loc=1, prop={'size':12})
          figname = os.environ['SPM'] + '/colors/StellarLocus.%s-%s.vs.%s-%s.after.color.calibration.png' % (self.lx1,self.lx2,self.ly1,self.ly2 )
          P.savefig(figname)
          P.close()
          
         
from matplotlib.ticker import NullFormatter,MaxNLocator          
class PlotStellarLocusKDEJoin(object):
      def __init__(self, icolorX, icolorY,fcolorX, fcolorY, colorpX, colorpY , dx, dy, excluded_points, lcolorx1, lcolorx2, lcolory1, lcolory2, correspondence_Matrix, plot_label=True):
	
          self.exarr=excluded_points #scatter points excluded by kde
          self.icolorx=icolorX #initial stellar locus without rejection by kde
          self.icolory=icolorY
          self.fcolorx=fcolorX #final stellar locus after rejection of some points by kde
          self.fcolory=fcolorY
          self.colorpx=colorpX #pickles stellar locus color in x direction
          self.colorpy=colorpY
          r=np.arange(self.icolorx.shape[0])
          self.arr=np.setxor1d(r,self.exarr) #obtain the included points
          
          self.lx1=lcolorx1 #first band name used to make color in x direction
          self.lx2=lcolorx2 #second band is used to make color in x direction
          self.ly1=lcolory1 #first band name used to make color in y direction
          self.ly2=lcolory2 #second band is used to make color in y direction
          self.M=correspondence_Matrix
          #just get the indicies where M_{ij} is greater than 0.99
          correspondence_indicies = np.where(self.M > 0.99)
          self.colorx_corr=self.fcolorx[correspondence_indicies[0]]
          self.colory_corr=self.fcolory[correspondence_indicies[0]]
          self.colorpx_corr=self.colorpx[correspondence_indicies[1]]
          self.colorpy_corr=self.colorpy[correspondence_indicies[1]]      
          self.PlotLabels=plot_label #Decide whether I want label in the plot or not
          self.dx=dx #The translation in the x direction computed from SPM method
          self.dy=dy#The translation in the y direction computed from SPM method
          self.xmin=np.minimum(min(self.icolorx),min(self.colorpx))-0.05
          self.xmax=np.maximum(max(self.icolorx),max(self.colorpx))+0.05
          self.ymin=np.minimum(min(self.icolory),min(self.colorpy))-0.05
          self.ymax=np.maximum(max(self.icolory),max(self.colorpy))+0.05
          
      def __repr__(self):
          return 'print the stellar locus in %s-%s vs. %s-%s diagram' % (self.lx1 ,self.lx2, self.ly1,self.ly2)   
      
      def plot_colors(self):
          fig=P.figure(1, figsize=(9,9), dpi=600)
          ax = fig.add_subplot(311, aspect=1)
          X=np.vstack((self.icolorx, self.icolory)).T
          data = pandas.DataFrame(X, columns=["X", "Y"])
          #seaborn.kdeplot(data.X,data.Y,bw='scott',shade=False, cmap="Purples_d") 
          flatui = ["white","#ffffff", "#ffffe4","#caa0ff","#c79fef","#bf77f6","#703be7","#601ef9","#6241c7"]
          cmap = seaborn.blend_palette(flatui, as_cmap=True)
          ax.set_aspect(abs((self.xmax-self.xmin)/(self.ymax-self.ymin)))
          ax.set_xlim(self.xmin,self.xmax)
          ax.set_ylim(self.ymin,self.ymax)
          seaborn.kdeplot(data.X,data.Y,bw='scott',shade=True,clip=[(self.xmin,self.xmax), (self.ymin,self.ymax)], ax=ax, cmap=cmap,lw=0.1, cut=0, shade_lowest=False) 
          ax.tick_params(axis='both', which='major', direction='in', length=6, width=2)
          ax.scatter(self.icolorx[self.arr], self.icolory[self.arr] ,marker='p',s=13, color='b', alpha=0.65,edgecolor='k')
          ax.scatter(self.icolorx[self.exarr], self.icolory[self.exarr],marker='p', s=13,c='y', alpha=0.55,edgecolor='k',lw = 0.6)
          #ax.scatter(self.colorpx, self.colorpy, s=13, c='r', marker='*', edgecolor='r',label='Pickles stars')
          #ax.set_xlabel("%s - %s"%(self.lx1,self.lx2), size='medium')
          ax.set_ylabel("%s - %s"%(self.ly1,self.ly2), size='medium')
          #ax.set_aspect('auto')
          for label in (ax.get_xticklabels() + ax.get_yticklabels()):
              label.set_fontsize(7)
          if self.PlotLabels:
             ax.legend(loc=1, prop={'size':12})
          ax.xaxis.set_major_formatter( NullFormatter() )   
          ax.yaxis.set_major_locator(MaxNLocator(prune='both'))
          ax = fig.add_subplot(312, aspect=1)
          ax.tick_params(axis='both', which='major', direction='in', length=6, width=2)
          ax.scatter(self.icolorx[self.arr], self.icolory[self.arr] ,marker='p',s=13, color='b', alpha=0.65,edgecolor='k')
          ax.scatter(self.icolorx[self.exarr], self.icolory[self.exarr],marker='p', s=13,c='y', alpha=0.55,edgecolor='k',lw = 0.6)
          ax.scatter(self.colorpx, self.colorpy, s=13, c='r', marker='*', edgecolor='r',label='Pickles stars')
          for i in range(len(self.colorx_corr)):
              ax.annotate("",
                          xy=(self.colorpx_corr[i], self.colorpy_corr[i]), xycoords='data',
                          xytext=(self.colorx_corr[i], self.colory_corr[i]), textcoords='data',
                          arrowprops=dict(arrowstyle="-", lw=0.3,
                          connectionstyle="arc3"), 
                          color='0.7'
                          )
          #ax.set_xlabel("%s - %s"%(self.lx1,self.lx2), size='medium')
          ax.set_ylabel("%s - %s"%(self.ly1,self.ly2), size='medium')
          #ax.set_aspect('auto')
          ax.set_aspect(abs((self.xmax-self.xmin)/(self.ymax-self.ymin)))
          ax.set_xlim(self.xmin,self.xmax)
          ax.set_ylim(self.ymin,self.ymax)
          for label in (ax.get_xticklabels() + ax.get_yticklabels()):
              label.set_fontsize(7)
          if self.PlotLabels:
             ax.legend(loc=1, prop={'size':12})
          ax.xaxis.set_major_formatter( NullFormatter() )
          ax.yaxis.set_major_locator(MaxNLocator(prune='both'))  
          ax = fig.add_subplot(313, aspect=1)
          ax.tick_params(axis='both', which='major', direction='in', length=6, width=2)
          ax.scatter(self.icolorx[self.arr]-self.dx, self.icolory[self.arr]-self.dy ,marker='p',s=13, color='b', alpha=0.65,edgecolor='k')
          ax.scatter(self.icolorx[self.exarr]-self.dx, self.icolory[self.exarr]-self.dy,marker='p',s=13,c='y', alpha=0.55,edgecolor='k',lw = 0.6)
          ax.scatter(self.colorpx, self.colorpy, s=13, c='r', marker='*', edgecolor='r',label='Pickles stars')
          ax.set_xlabel("%s - %s"%(self.lx1,self.lx2), size='medium')
          ax.set_ylabel("%s - %s"%(self.ly1,self.ly2), size='medium')
          #ax.set_aspect('auto')
          ax.set_aspect(abs((self.xmax-self.xmin)/(self.ymax-self.ymin)))
          ax.set_xlim(self.xmin,self.xmax)
          ax.set_ylim(self.ymin,self.ymax)
          for label in (ax.get_xticklabels() + ax.get_yticklabels()):
              label.set_fontsize(7)
          if self.PlotLabels:
             ax.legend(loc=1, prop={'size':12})
          ax.xaxis.set_major_locator(MaxNLocator(prune='both'))
          ax.yaxis.set_major_locator(MaxNLocator(prune='both'))
          fig.subplots_adjust(wspace=0,hspace=0)
          figname = os.environ['SPM'] + '/colors/StellarLocus.%s-%s.vs.%s-%s.color.calibration.paper.style.eps' % (self.lx1,self.lx2,self.ly1,self.ly2 )
          P.savefig(figname)
          P.close()

#Reference:http://stackoverflow.com/questions/5967500/how-to-correctly-sort-a-string-with-a-number-inside
def atoi(text):
    return int(text) if text.isdigit() else text

def natural_keys(text):
    '''
    alist.sort(key=natural_keys) sorts in human order
    http://nedbatchelder.com/blog/200712/human_sorting.html
    (See Toothy's implementation in the comments)
    '''
    return [ atoi(c) for c in re.split('(\d+)', text) ]
  
class Get_Model_Magnitudes(object): 
      """This class uses the filter set and 
         convolve the SDSS stellar spectra with the given filters 
      """
  
      def __init__(self, confname, outname):

          self.inputfile  = os.environ['SPM'] + '/config/'+confname
          if not os.path.exists('StellarLocus'):
             os.makedirs('StellarLocus')
          self.output     = os.environ['SPM'] + '/StellarLocus/'+ outname
          self.speed_light= 299792458e10
          
      def __repr__(self):

          return 'The configuration file contains the name of filters:\n%s' %(self.inputfile)   
          
      def ConfigFileParser(self):

          self.filter_info={}
          self.valtypes=[]
          self.NameFilters=[]
          with open(self.inputfile,'r') as f:
               for line in f:
                   LineSplit = line.split()
                   self.filter_info[LineSplit[0]]=(LineSplit[1],LineSplit[2])
                   self.NameFilters.append(LineSplit[0])
                   self.valtypes.append((LineSplit[0],'f4'))
          print self.NameFilters
      def read_filters(self):

          self.FiltersPack=[]
          for filters, filterfile in self.filter_info.iteritems():
              filter_dir = os.environ['SPM'] + '/filters/'+ filterfile[0]
              filt = np.genfromtxt(filter_dir)
              step= filt[1,0] - filt[0,0]
              if filt[0,0]>filt[-1,0]:
                 #Try to sort the content of filter files based on wavelengths on the first column
                 nfilt=filt.tolist()
                 nfilt.reverse()
                 filt = np.array(nfilt)
              filterSpline = scipy.interpolate.interp1d(filt[:,0], filt[:,1], 
                                       bounds_error = False, 
                                       fill_value = 0.)   
              self.FiltersPack.append({'wavelength':filt[:,0],'response':filt[:,1],'spline':copy(filterSpline),'step':copy(step),'name':copy(filters),'center wavelength': scipy.average(filt[:,0],weights=filt[:,1])})
              
              
      def convolve_spectra_stars(self):
          #This function returns the stellar magnitudes in the AB system
          slist= glob.glob(os.environ['SPM'] + '/stellar_spectra/*.dat')
          slist.sort(key=natural_keys)
          stellar_locus=[]
          for specname in slist:
              spectrum=np.genfromtxt(specname)
              spectrum_index = re.search('/stellar_spectra/(.+?).dat', specname).group(1)
              p=[1.,0,0,0]
              spectra=[[spectrum]]
              mags={}
              for keys in self.NameFilters:
                  for i in range(len(self.FiltersPack)):
                      if self.FiltersPack[i]['name']==keys:
                         specall = np.zeros(len(spectra[0][0][:,1]))
                         val = 0 
                         for coeff,specfull  in [[p[0],spectra[0]]]: #,[p[1],spectra[1]],[1.-p[0]-p[1],spectra[2]]]: 
                             spec = specfull[0]
                             specStep = spec[1:,0] - spec[0:-1,0] # wavelength increment                   
            
                             resampFilter = self.FiltersPack[i]['spline'](spec[:,0]) # define an interpolating function            
                             ''' need to multiply by polynomial '''
                             val += abs(coeff)*sum(specStep * resampFilter[:-1] * spec[:-1,0] * spec[:-1,1]) # photon counting!!

                         logEff = np.log10(val)                                        
                         logNorm = np.log10(sum(resampFilter[:-1]*self.speed_light*specStep/spec[:-1,0]))
                         mag = 2.5*(logNorm - logEff) # to calculated an AB magnitude    
                         mags[self.FiltersPack[i]['name']]=mag
              stellar_locus.append(mags) 
          Stellar_DataFrame=pd.DataFrame(stellar_locus)
          Stellar_DataFrame = Stellar_DataFrame[self.NameFilters]
          f = open(self.output, 'w')
          headers = Stellar_DataFrame.columns.tolist()
          f.write("#"+"\t".join(headers)+"\n")
          Stellar_DataFrame.to_csv(f, sep='\t', index=False, header=False, encoding='utf-8')
          f.close()
          return stellar_locus
