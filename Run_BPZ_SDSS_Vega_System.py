# -*- coding: utf-8 -*-
import subprocess as S
import sys, re, os, math 
import numpy as np
from numpy.lib.recfunctions import append_fields
import csv
import scipy.integrate
import shutil
from scipy.spatial import cKDTree
import os.path
from collections import Iterable
import pyfits as fits
from astropy import units as u
from astropy.coordinates import SkyCoord
from scipy.ndimage.interpolation import map_coordinates
from astropy.io import ascii
from astropy.table import Column
import pandas as pd
#maximum number of decimal digits that can be faithfully represented in a float
print sys.float_info.dig
#Define functions:
def extrema(x, max = True, min = True, strict = False, withend = False):
	"""
	This function will index the extrema of a given array x.
	
	Options:
		max		If true, will index maxima
		min		If true, will index minima
		strict		If true, will not index changes to zero gradient
		withend	If true, always include x[0] and x[-1]
	
	This function will return a tuple of extrema indexies and values
	"""
	
	# This is the gradient
	dx = np.zeros(len(x))
	dx[1:] = np.diff(x)
	dx[0] = dx[1]
	
	# Clean up the gradient in order to pick out any change of sign
	dx = np.sign(dx)
	
	# define the threshold for whether to pick out changes to zero gradient
	threshold = 0
	if strict:
		threshold = 1
		
	# Second order diff to pick out the spikes
	d2x = np.diff(dx)
	
	if max and min:
		d2x = abs(d2x)
	elif max:
		d2x = -d2x
	
	# Take care of the two ends
	if withend:
		d2x[0] = 2
		d2x[-1] = 2
	
	# Sift out the list of extremas
	ind = np.nonzero(d2x > threshold)[0]
	return ind, x[ind]
	
def local_maxima(r,pdf):
    peaks=np.zeros((2,len(extrema(pdf, max = True, min = False, strict = False, withend = False)[0])),float)
    peaks[0,:]=r[extrema(pdf, max = True, min = False, strict = False, withend = False)[0]]
    peaks[1,:]=extrema(pdf, max = True, min = False, strict = False, withend = False)[1]
    idx=np.argsort(peaks[1,:]) 
    new_peaks=peaks[:,idx]
    #print new_peaks
    z=[]
    if (new_peaks.shape[1]< 3):
        if (new_peaks.shape[1]==1):
	    z.append(new_peaks[0,:])
	if (new_peaks.shape[1]>1): 
	   if (abs(new_peaks[0,1]-new_peaks[0,0])>0.02):
	      z.append(new_peaks[0,0])
	      z.append(new_peaks[0,1])
	   else:
	      z.append(new_peaks[0,0])	      
    else:
        i=0
        for j in range(new_peaks.shape[1]-1,0,-1):
	    if(i==0):
	       z.append(new_peaks[0,j])
	       i+=1
	    elif (i<2):
	        if (abs(new_peaks[0,j+1]-new_peaks[0,j])>0.02):
                   z.append(new_peaks[0,j])
                   i+=1
    return z
##----------------------------------------------------------------------------------------

def get_ebv_from_map(coordinates, interpolate=True, order=1):
    """Get E(B-V) value(s) from Schlegel, Finkbeiner, and Davis 1998 extinction
    maps at the given coordinates.

    Parameters
    ----------
    coordinates : astropy `~astropy.coordinates.coordsystems.SphericalCoordinatesBase` or tuple/list.
        If tuple/list, treated as (RA, Dec) in degrees the ICRS (e.g., "J2000")
        system. RA and Dec can each be float or list or numpy array.
    mapdir : str, optional
        Directory in which to find dust map FITS images, which must be named
        ``SFD_dust_4096_[ngp,sgp].fits``. If `None` (default), the value of
        the SFD_MAP_DIR configuration item is used. By default, this is ``'.'``.
        The value of SFD_MAP_DIR can be set in the configuration file,
        typically located in ``$HOME/.astropy/config/sncosmo.cfg``.
    interpolate : bool
        Interpolate between the map values using
        `scipy.ndimage.map_coordinates`.
    order : int
        Interpolation order, if interpolate=True. Default is 1.

    Returns
    -------
    ebv : float or `~numpy.ndarray`
        Specific extinction E(B-V) at the given locations.

    """
    #
    # Get mapdir
    #
    mapdir='./Dust_Map/maps/'
    fname = os.path.join(mapdir, 'SFD_dust_4096_{0}.fits')
    #
    # Parse input 
    ra, dec = coordinates
    coordinates = SkyCoord(ra=ra, dec=dec, unit=(u.degree, u.degree))
    #
    # Convert to galactic coordinates.
    coordinates = coordinates.galactic
    l = coordinates.l.radian
    b = coordinates.b.radian
    #
    # Check if l, b are scalar
    return_scalar = False
    if not isinstance(l, Iterable):
        return_scalar = True
        l, b = np.array([l]), np.array([b])
    #
    # Initialize return array
    ebv = np.empty_like(l)
    #
    # Treat north (b>0) separately from south (b<0).
    for n, idx, ext in [(1, b >= 0, 'ngp'), (-1, b < 0, 'sgp')]:
        #
        if not np.any(idx): continue
        hdulist = fits.open(fname.format(ext))
        mapd = hdulist[0].data
        #
        # Project from galactic longitude/latitude to lambert pixels.
        # (See SFD98).
        npix = mapd.shape[0]        
        x = (npix / 2 * np.cos(l[idx]) * np.sqrt(1. - n*np.sin(b[idx])) +
             npix / 2 - 0.5)
        y = (-npix / 2 * n * np.sin(l[idx]) * np.sqrt(1. - n*np.sin(b[idx])) +
             npix / 2 - 0.5)
        #        
        # Get map values at these pixel coordinates.
        if interpolate:
            ebv[idx] = map_coordinates(mapd, [y, x], order=order)
        else:
            x=np.round(x).astype(np.int)
            y=np.round(y).astype(np.int)
            ebv[idx] = mapd[y, x]
        #    
        hdulist.close()
        #
    if return_scalar:
        return ebv[0]
    return ebv
##----------------------------------------------------------------------------------------
    

def correct_zeropoints( galactic_dust=True):
    """
    30th April 2017
    zero-points are measured based on this code:
    
    This function corrects for magnitude zero-point offsets as well as galactic dust extinction
    and compute photometric redshifts 
    """
    datadir=os.getcwd()+"/data/"
    resdir=os.getcwd()+"/results/"
    ZP=np.loadtxt(datadir+"zero-points.offsets.CDFS.catalogue.SDSS.Vega.2017.update.asc")
    print "zero-point offsets:"
    print ZP
    Gal         =  ascii.read(datadir+"CDFS_Galaxy.asc")
    columns     = Gal.colnames
    print columns
    print "size of CDFS catalogue:"
    print len(Gal)
    ra=Gal['RAJ2000']
    dec=Gal['DEJ2000']
    #get the name of columns contain mag values
    MB=[]; mbands=[]
    BB=[]; bbands=[]
    for value in columns:
        m = re.match('W(.+?)F', value)    
        if (m != None):
           MB.append(m.group(1))
           mbands.append(value)
        n = re.match('(.?)F', value)
        if (n != None):
           BB.append(n.group(1))
           bbands.append(value)
    filters    = BB + MB
    #This list contains the name of columns in the original catalogue
    mag_columns= bbands+mbands  

    #Av=[3.836, 3.070, 2.542, 2.058, 1.313, 3.466, 3.082, 2.882, 2.652, 2.367, 2.226, 2.066, 1.872, 1.652, 1.423, 1.298, 1.156]
    Av=[4.591, 3.660, 3.031, 2.454, 1.553, 4.105, 3.651, 3.430, 3.150, 2.818, 2.650, 2.461, 2.230, 1.970, 1.679, 1.630, 1.383]
    ebv_value=get_ebv_from_map((ra, dec), interpolate=True, order=1)
    #modify photometry based on measured zero-point offsets from SPM method

    for j in range(len(filters)):
        ColName     = mag_columns[j]
        idx=np.where(Gal[ColName]>0.0)[0]
        if (galactic_dust):
	   Gal[ColName][idx]=Gal[ColName][idx]-ZP[j]-ebv_value[idx]*Av[j]# Including dust extinction 
	else:
	   Gal[ColName][idx]=Gal[ColName][idx]-ZP[j]
    if (galactic_dust):
       outname=datadir+'Correct_CDFS_Galaxies_Photometry_ZP_Recalib_Via_SPM_VEGA_SDSS_Stellar_Locus_Galactic_Dust_Extinction_Correction.cat'
       print outname
    else:
       outname=datadir+'Correct_CDFS_Galaxies_Photometry_ZP_Recalib_Via_SPM_VEGA_SDSS_Stellar_Locus_No_Galactic_Dust_Extinction_Correction.cat'
       print outname
    ascii.write(Gal, outname, names=Gal.colnames, format='commented_header')   

        
    #Make free memory
    del Gal 

    
    sys.stdout.flush()
    #------------------------------------main--------------------------------
    READYFORBPZ=outname
    print outname
    os.environ["BPZPATH"] = "/vol/aibn84/data2/zahra/Zahra/BPZ/bpz-1.99.3/"
    BPZPATH='/vol/aibn84/data2/zahra/Zahra/BPZ/bpz-1.99.3/'
    os.environ["BPZPATH"]
    os.environ["NUMERIX"]='numpy'
    #COL01='/users/zahra/Desktop/MYPAPER/SPA/data/CDFS_Wolf2004_catalogue.columns'
    COL01='./config/BPZ_CDFS.columns'
    if (galactic_dust):
       BPZCAT=datadir+'Photometric_redshift_BPZ_CDFS_SPM_ZP_Offset_Recalibration_SDSS_Stellar_Locus_Galactic_Dust_Extinction_Correction.cvs'
    else:
      BPZCAT=datadir+'Photometric_redshift_BPZ_CDFS_SPM_ZP_Offset_Recalibration_SDSS_Stellar_Locus_No_Galactic_Dust_Extinction_Correction.cvs'
    SED=BPZPATH+'SED/CWWSB_capak.list'
    CMD='python '+BPZPATH+'bpz.py '+READYFORBPZ+' -COLUMNS '+COL01+' -OUTPUT '+BPZCAT+' -SPECTRA '+SED+' -ZMAX 1.5 -ZMIN 0.0001 -DZ 0.001 -PRIOR DLS -INTERP 4 -VERBOSE no -PROBS_LITE '+resdir+'posterior_probability_redshift_CDFS.cat' 
    print>>sys.stderr, CMD+"\n"
    S.call([CMD], shell=True)# shell=True, stdout=S.PIPE)      
    PDF=np.loadtxt(resdir+'posterior_probability_redshift_CDFS.cat')
    dz=0.001
    z=np.arange(0.0001,1.501,0.001)
    ZM=np.zeros(PDF.shape[0],float)
    Zf=np.zeros(PDF.shape[0],float)
    Zs=np.zeros(PDF.shape[0],float)
    for i in range(PDF.shape[0]):
        Zc=0; Ztot=0.0
        for j in range(len(z)):
            Zc+=z[j]*PDF[i,j+1]*dz
            Ztot+=PDF[i,j+1]*dz
        ZM[i]=Zc/Ztot
        pdf=PDF[i,1:]
        if (len(local_maxima(z,pdf))==2):
           Zf[i],Zs[i]=local_maxima(z,pdf)
        elif (len(local_maxima(z,pdf))==1):    
           Zf[i]=local_maxima(z,pdf)[0]           
           Zs[i]=0.0
           print "only one maxima:"
           print i,ZM[i],Zf[i]
        else:
           print "No local maxima: "
           print i, local_maxima(z,pdf), np.max(pdf), np.min(pdf), len(np.where(pdf==np.min(pdf))[0])  
    print BPZCAT
   
    row=ascii.read(outname)
    print len(row.colnames)

    # 1 ID, 2 Z_B, 3 Z_B_MIN, 4 Z_B_MAX, 5 T_B, 6 ODDS, 7 Z_ML, 8 T_ML, 9 CHI-SQUARED, 10 M_0
    data=ascii.read(BPZCAT)
    zb=Column(data["Z_B"].data, name="Z_B")
    row.add_column(zb, index=len(row.colnames))
    print len(row.colnames)
    print row.colnames
    zbmin=Column(data["Z_B_MIN"].data, name="Z_B_MIN")
    row.add_column(zbmin, index=len(row.colnames))
    print row.colnames
    zbmax=Column(data["Z_B_MAX"].data, name="Z_B_MAX")
    row.add_column(zbmax, index=len(row.colnames))
    print row.colnames
    tb=Column(data["T_B"].data, name="T_B")
    row.add_column(tb, index=len(row.colnames))
    print row.colnames
    odds=Column(data["ODDS"].data, name="ODDS")
    row.add_column(odds, index=len(row.colnames))
    print row.colnames
    chi2=Column(data["CHI-SQUARED"].data, name="CHI-SQUARED")
    row.add_column(chi2, index=len(row.colnames))
    print len(row.colnames)
    print row.colnames
    zm=Column(ZM, name="Z_M")
    row.add_column(zm, index=len(row.colnames))
    print row.colnames
    zf=Column(Zf, name="Z_fp")
    row.add_column(zf, index=len(row.colnames))
    print row.colnames
    zs=Column(Zs, name="Z_sp")
    row.add_column(zs, index=len(row.colnames))
    print row.colnames
    print len(row.colnames)
    #writing the columns are two files in one output file
    if (galactic_dust):
       filename=datadir+'Final_CDFS_Photometric_Redshift_Catalogue_SPM_Photometry_Recalibration_SDSS_Stellar_Locus_Galactic_Dust_Extinction_Correction.cat'
    else:
       filename=datadir+'Final_CDFS_Photometric_Redshift_Catalogue_SPM_Photometry_Recalibration_SDSS_Stellar_Locus_No_Galactic_Dust_Extinction_Correction.cat'
    ascii.write(row, filename, names=row.colnames, format='commented_header')  
    del ZM
    del row
    del data
    print "Photometric redshift estimation is done!!"
    print "Combine CDFS catalogue with the VVDS spectroscopic catalogue"
    cat=np.genfromtxt("./data/VIMOS@VLT_CDFS.cat", names=True)
    data=np.genfromtxt(filename, names=True)
    coord=np.vstack((data['RAJ2000'],data['DEJ2000'])).T
    k = cKDTree(coord) # creating the KDtree using the Xpos and Ypos
    xyCenters = np.vstack((cat['RAdeg'],cat['DEdeg'])).T
    print(k.query(xyCenters))
    dists, indices = k.query(xyCenters)
    b=(cat['z']>0)*(dists<1e-03)
    new_indices=indices[b]
    similarX = np.take(data['RAJ2000'], new_indices)
    similarY = np.take(data['DEJ2000'], new_indices)
    new_coord=xyCenters[b]
    photoz=np.take(data['Z_B'], new_indices)
    specz=cat['z'][b]
    specz=np.array(specz.reshape((len(specz),)),dtype=[('Z_s',float)])
    GEMS=np.take(data,new_indices)
    my_appended_array = append_fields(GEMS,'Z_s',specz, usemask=False, dtypes=[float])
    if (galactic_dust):
        catalogue=datadir+'Final_CDFS_Photometric_Redshift_Catalogue_VVDS_SPECZ_SPM_Photometry_Recalibration_SDSS_Stellar_Locus_Galactic_Dust_Extinction_Correction.cat'
    else:
       catalogue=datadir+'Final_CDFS_Photometric_Redshift_Catalogue_VVDS_SPECZ_SPM_Photometry_Recalibration_SDSS_Stellar_Locus_No_Galactic_Dust_Extinction_Correction.cat'
    headers=list(my_appended_array.dtype.names)
    np.savetxt(catalogue, my_appended_array, fmt='%.4f', header='\t'.join(headers) + '\n',delimiter='\t', newline='\n')
if __name__ == "__main__":
   correct_zeropoints( galactic_dust=True)
   correct_zeropoints( galactic_dust=False)
