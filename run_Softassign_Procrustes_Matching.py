# -*- coding: utf-8 -*-
import numpy as np
import subprocess as S
import pylab as plt
import codecs
import PySPM as Softassign
import math, copy
#Give an address for your environmental variable here          
os.environ["SPM"] = "/users/zahra/development/SPM/"

def nCr(n,r):
    f = math.factorial
    return f(n) / f(r) / f(n-r)

def make_colors_whole(ID,input_cat,order=True):
    """
    Generates all the colour-colour spaces that you require as
    well as their filter Indices.
    """
    y=np.zeros((nCr(len(ID),3),3),int)
    color=np.zeros((input_cat.shape[0],y.shape[0],2),float)
    l=0
    flag=0
    for i in range(len(ID)-2):
        j=i+1
        while j<(len(ID)-1):
            for m in range(input_cat.shape[0]):
                color[m,l,flag]=input_cat[m,i]-input_cat[m,j]
            flag=1
            k=j+1
            while k<=(len(ID)-1):
                if flag !=1:
                   for m in range(input_cat.shape[0]):
                       color[m,l,flag]=input_cat[m,i]-input_cat[m,j]
                   flag=1
                for m in range(input_cat.shape[0]):
                    color[m,l,flag]=input_cat[m,j]-input_cat[m,k]
                y[l,0]=i
                y[l,1]=j
                y[l,2]=k
                l+=1 #index for keeping the identity of two color sets in diagram
                k+=1
                flag=0
            j+=1
    if order:
       return y,color
    else:
       return color

     
def make_colors(input_filters, input_cat, order=True):
    """
      This function first generates the indexes of filter combinations that 
      produce reliable colors  
      filters should be a dictionary with first element represents the order of data in the input_cat of that particular filter
      second value is the effective wavelength of the filter
      third value is the width of the given filter
      example:
      filt={"U38":[0, 365, 40],"B":[1, 455, 100],"V": [2, 540, 90],"R":[3, 650, 165],"I":[4, 850, 150],"MB420":[5, 420, 30],"MB464":[6, 465, 15],"MB485":[7, 486, 30],"MB518":[8, 518, 16],"MB571":[9, 571, 24],"MB604":[10, 605, 21],"MB646":[11, 646, 27],"MB696":[12, 696, 23],"MB753":[13, 752, 20],"MB815":[14, 816, 24],"MB856":[15, 856,14],"MB914":[16, 914, 27]}
    """
    filters=copy.deepcopy(input_filters)
    print filters
    for key, value in filters.iteritems():
        min_key=value[1]-value[2]/2
        max_key=value[1]+value[2]/2
        filters[key].append(min_key)
        filters[key].append(max_key)
    print filters
    y=np.zeros((1,3),int)
    flag=True
    for key1, value1 in filters.iteritems():            
        for key2, value2 in filters.iteritems():
            #if min wavelength of the second filter is greater than the effective wavelength of the first filter
            if ((value2[3]> value1[4]) and (value2[1]<696)):
               if (value1[2]<40):
                  if (abs(value1[0]-value2[0])<2):
                     flag=False
               if flag:
                  #exclude medium-bands which are neighbors 
                  if (y.shape[0]==1):
                     y[y.shape[0]-1,0]=value1[0]
                     y[y.shape[0]-1,1]=value2[0] 
                  else:
                     y= np.vstack((y,np.zeros((1,3),int)))  
                     y[y.shape[0]-1,0]=value1[0]
                     y[y.shape[0]-1,1]=value2[0]      
                  n=0         
                  state=True    
                  for key3, value3 in filters.iteritems(): 
                      if value3[3]> value2[4]:
                        if (value2[2]<40):
                           if ((abs(value2[0]-value3[0])<2) or (value3[2]>40)):
                              state=False
                        if state:
                           #exclude medium-bands which are neighbors or broad-bands
                           if (n==0):
                              y[y.shape[0]-1,2]=value3[0]
                           else:
                              y= np.vstack((y,np.zeros((1,3),int)))  
                              y[y.shape[0]-1,0]=value1[0]
                              y[y.shape[0]-1,1]=value2[0]      
                              y[y.shape[0]-1,2]=value3[0]
                           n+=1 
                      state=True  
               if (y[y.shape[0]-1,2]==0):
                   #delete the row if the last element of that row is zero
                   y = np.delete(y, -1, axis=0)
            flag=True
    #generate colours related to the index of y
    color=np.zeros((input_cat.shape[0],y.shape[0],2),float)
    for i in range(y.shape[0]):
        color[:,i,0]=input_cat[:,y[i,0]]-input_cat[:,y[i,1]]
        color[:,i,1]=input_cat[:,y[i,1]]-input_cat[:,y[i,2]]
    if order:
       return y,color
    else:
       return color

if __name__ == "__main__":
    "Running SPM by initializing its parameters"

    #make data and plot directory
    data_dir = 'data'
    plot_dir = 'plots'   # output directory

    for d in (plot_dir, data_dir):
        shcommand = 'mkdir -p %s' % (d)
        S.call(shcommand, shell=True)

    #Name of filters
    ID=['U38','B','V','R','I','MB420','MB464','MB485','MB518','MB571','MB604','MB646','MB696','MB753','MB815','MB856','MB914']
    #A dictionary contains information about filters
    filts = {"U38":[0, 365, 40],"B":[1, 455, 100],"V": [2, 540, 90],"R":[3, 650, 165],"I":[4, 850, 150],"MB420":[5, 420, 30],"MB464":[6, 465, 15],"MB485":[7, 486, 30],"MB518":[8, 518, 16],"MB571":[9, 571, 24],"MB604":[10, 605, 21],"MB646":[11, 646, 27],"MB696":[12, 696, 23],"MB753":[13, 752, 20],"MB815":[14, 816, 24],"MB856":[15, 856,14],"MB914":[16, 914, 27]}
    #-------------Reading data from source files------------------
    #input uncalibrated stellar catalogue
    
    InputCat=np.genfromtxt('./data/CDFS_Star.asc', unpack=True)
    cat=np.transpose(InputCat)[:,15:32]
    error=np.transpose(InputCat)[:,32:]
    print cat.shape
    print "original number of stars in the catalogue:",cat.shape[0]

    ##stellar locus from SDSS in AB magnitude system
    #configuration file name 
    confname="COMBO17.columns"
    c=Softassign.Get_Model_Magnitudes("COMBO17.columns","BigMacs_SDSS_standard_stars_AB.csv")
    c.ConfigFileParser()
    print "Detail info about filters:"
    print c.filter_info
    c.read_filters()
    print "Final results after convolution of templates with filters:"
    print c.convolve_spectra_stars()
    print

    X = []
    with codecs.open(os.environ["SPM"]+"/StellarLocus/"+"BigMacs_SDSS_standard_stars_AB.csv", encoding='utf-8-sig') as f:
        for line in f:
            s = line.split()
            X.append([float(s[i]) for i in range(len(s))])        
    #values needed to be added to magnitudes in seventeen filters to convert from AB to Vega system 
    ABtoVEGA=[+0.77,-0.13,-0.02,+0.19,+0.49,-0.19,-0.18,-0.06,-0.06,+0.04,+0.10,+0.22,+0.27,+0.36,+0.45,+0.56,+0.50]

    SDSS=np.asarray(X)
    pickles=np.zeros_like(SDSS)
    for i in range(len(ID)):
        for j in range(SDSS.shape[0]):
            pickles[j,i]=SDSS[j,i]-ABtoVEGA[i]

    #-------------Doing some refinements-----------------------------------
    b=np.ones_like(cat[:,0], dtype=bool)
    #implementing some cut to discard low signal-to-noise stellar objects 
    for i in range(cat.shape[1]):
        b1=np.logical_and(error[:,i]>0, error[:,i]<0.025)
        b*=b1
    

    cat=cat[b,:]     
    print "final number of stars in the catalogue:",cat.shape[0]
    print "preparing the color combinations for pickles catalogue"
    #y,color=make_colors(filts,cat,order=True)
    #color_p=make_colors(filts,pickles,order=False)
    y,color=make_colors_whole(ID,cat,order=True)
    color_p=make_colors_whole(ID,pickles,order=False)
    print "filter combinations which will be used for color calibration:\n",y
    #-----------------Outlier detection---------------------------------
    #color combinations that have been used to determine outliers in the color-color spaces
    outliers=np.zeros((cat.shape[0],9,2),float)
    outliers[:,0,0]=cat[:,6]-cat[:,8] #MB464-MB518
    outliers[:,0,1]=cat[:,8]-cat[:,10]#MB518-MB604
    #
    outliers[:,1,0]=cat[:,14]-cat[:,15] #MB815-MB856
    outliers[:,1,1]=cat[:,15]-cat[:,16]#MB856-MB914
    #
    outliers[:,2,0]=cat[:,8]-cat[:,10] #MB518-Mb604
    outliers[:,2,1]=cat[:,10]-cat[:,11]#MB604-MB646
    #
    outliers[:,3,0]=cat[:,0]-cat[:,8] #U38-MB518
    outliers[:,3,1]=cat[:,8]-cat[:,10]#MB518-MB604
    #
    outliers[:,4,0]=cat[:,5]-cat[:,7] #MB420-MB485
    outliers[:,4,1]=cat[:,7]-cat[:,14]#MB485-MB815
    #
    outliers[:,5,0]=cat[:,1]-cat[:,8] #B-MB518
    outliers[:,5,1]=cat[:,8]-cat[:,13] #MB518-MB753
    #
    outliers[:,6,0]=cat[:,1]-cat[:,2] #B-V                
    outliers[:,6,1]=cat[:,2]-cat[:,10] #V-MB604     
    #
    outliers[:,7,0]=cat[:,6]-cat[:,7] #MB464-MB485
    outliers[:,7,1]=cat[:,7]-cat[:,15] #MB485-MB856
    #
    outliers[:,8,0]=cat[:,6]-cat[:,2] #MB464-V
    outliers[:,8,1]=cat[:,2]-cat[:,10] #V-MB604
    #running code which uses kernel density estimation method for outlier rejection
    excluded=Softassign.refineStellarLocusKDE(outliers[:,:,0],outliers[:,:,1],ratio=10)
    print "excluding scatter points!!"
    print "before reject scatter points:", cat.shape[0]
    r=np.arange(color.shape[0])
    new_r=np.setxor1d(r,excluded)
    new_color=np.zeros((len(new_r),y.shape[0],2),float)
    new_color[:,:,0]=color[new_r,:,0]
    new_color[:,:,1]=color[new_r,:,1]
    cat=cat[new_r,:]
    print "after reject scatter points:", cat.shape[0]
    # -----rejecting a part of redward of stellar locus
    print "reject scatter points in pickles catalogue too!"
    print "number of objects in pickles before any rejection:",pickles.shape    
    #19th April 2017
    ref=np.zeros((pickles.shape[0],2),float)
    ref[:,0]=pickles[:,0]-pickles[:,2]
    ref[:,1]=pickles[:,2]-pickles[:,4]
    new_ex=(ref[:,1]<3.5)

    new_color_p=np.zeros((np.sum(new_ex),y.shape[0],2),float)
    new_color_p[:,:,0]=color_p[new_ex,:,0]
    new_color_p[:,:,1]=color_p[new_ex,:,1]
    pickles=pickles[new_ex,:]

    print "number of objects in pickles after rejection:",pickles.shape
    #-----------------------making a model for SPM-----------------------------------
    print "making a model for SPM with combination of different color-color spaces"
    dd  = np.zeros((cat.shape[0],2,20),float)
    ref = np.zeros((pickles.shape[0],2,20),float)
    #'0:U38','1:B','2:V','3:R','4:I','5:MB420','6:MB464','7:MB485','8:MB518','9:MB571','10:MB604','11:MB646','12:MB696','13:MB753','14:MB815','15:MB856','16:MB914'
    #
    dd[:,0,0]=cat[:,0]-cat[:,9] #U38-MB571
    dd[:,1,0]=cat[:,9]-cat[:,15] #MB571-MB856
    ref[:,0,0]=pickles[:,0]-pickles[:,9]
    ref[:,1,0]=pickles[:,9]-pickles[:,15]
    #
    dd[:,0,1]=cat[:,1]-cat[:,7] #B-MB485
    dd[:,1,1]=cat[:,7]-cat[:,15] #MB485-MB856
    ref[:,0,1]=pickles[:,1]-pickles[:,7]
    ref[:,1,1]=pickles[:,7]-pickles[:,15]
    #60%
    dd[:,0,2]=cat[:,1]-cat[:,2] #B-V
    dd[:,1,2]=cat[:,2]-cat[:,10] #V-MB604
    ref[:,0,2]=pickles[:,1]-pickles[:,2]
    ref[:,1,2]=pickles[:,2]-pickles[:,10]
    #60%
    dd[:,0,3]=cat[:,2]-cat[:,6] #V-MB464
    dd[:,1,3]=cat[:,6]-cat[:,8] #MB464-MB518
    ref[:,0,3]=pickles[:,2]-pickles[:,6]
    ref[:,1,3]=pickles[:,6]-pickles[:,8]
    #
    dd[:,0,4]=cat[:,2]-cat[:,10] #V-MB604                       
    dd[:,1,4]=cat[:,10]-cat[:,13] #MB604-MB753        
    ref[:,0,4]=pickles[:,2]-pickles[:,10]                            
    ref[:,1,4]=pickles[:,10]-pickles[:,13]                           
    #
    dd[:,0,5]=cat[:,2]-cat[:,10] #V-MB604
    dd[:,1,5]=cat[:,10]-cat[:,3] #MB604-R
    ref[:,0,5]=pickles[:,2]-pickles[:,10]
    ref[:,1,5]=pickles[:,10]-pickles[:,3]
    #
    dd[:,0,6]=cat[:,3]-cat[:,6] #R-MB464                       
    dd[:,1,6]=cat[:,6]-cat[:,7] #MB464-MB485        
    ref[:,0,6]=pickles[:,3]-pickles[:,6]                            
    ref[:,1,6]=pickles[:,6]-pickles[:,7]                           
    #
    dd[:,0,7]=cat[:,4]-cat[:,5] #I-MB420
    dd[:,1,7]=cat[:,5]-cat[:,6] #MB420-MB464
    ref[:,0,7]=pickles[:,4]-pickles[:,5]
    ref[:,1,7]=pickles[:,5]-pickles[:,6]
    #
    dd[:,0,8]=cat[:,4]-cat[:,9] #I-MB571
    dd[:,1,8]=cat[:,9]-cat[:,2] #MB571-V
    ref[:,0,8]=pickles[:,4]-pickles[:,9]
    ref[:,1,8]=pickles[:,9]-pickles[:,2]
    #
    dd[:,0,9]=cat[:,5]-cat[:,2] #MB420 - V                    
    dd[:,1,9]=cat[:,2]-cat[:,10] #V - MB604       
    ref[:,0,9]=pickles[:,5]-pickles[:,2]                            
    ref[:,1,9]=pickles[:,2]-pickles[:,10]
    #
    dd[:,0,10]=cat[:,5]-cat[:,8] #MB420-MB518
    dd[:,1,10]=cat[:,8]-cat[:,9] #MB518-MB571
    ref[:,0,10]=pickles[:,5]-pickles[:,8]
    ref[:,1,10]=pickles[:,8]-pickles[:,9]
    #
    dd[:,0,11]=cat[:,5]-cat[:,7] #MB420-MB485
    dd[:,1,11]=cat[:,7]-cat[:,8] #MB485-MB518
    ref[:,0,11]=pickles[:,5]-pickles[:,7]
    ref[:,1,11]=pickles[:,7]-pickles[:,8]
    #
    dd[:,0,12]=cat[:,5]-cat[:,9] #MB420-MB571
    dd[:,1,12]=cat[:,9]-cat[:,13] #MB571-MB753
    ref[:,0,12]=pickles[:,5]-pickles[:,9]
    ref[:,1,12]=pickles[:,9]-pickles[:,13]
    #
    dd[:,0,13]=cat[:,6]-cat[:,8] #MB464-MB518                       
    dd[:,1,13]=cat[:,8]-cat[:,15] #MB518-MB856        
    ref[:,0,13]=pickles[:,6]-pickles[:,8]                            
    ref[:,1,13]=pickles[:,8]-pickles[:,15]                           
    #
    dd[:,0,14]=cat[:,6]-cat[:,2] #MB464-V
    dd[:,1,14]=cat[:,2]-cat[:,10] #V-MB604
    ref[:,0,14]=pickles[:,6]-pickles[:,2]
    ref[:,1,14]=pickles[:,2]-pickles[:,10]
    #
    dd[:,0,15]=cat[:,7]-cat[:,8] #MB485-MB518
    dd[:,1,15]=cat[:,8]-cat[:,10] #MB518-MB604
    ref[:,0,15]=pickles[:,7]-pickles[:,8]
    ref[:,1,15]=pickles[:,8]-pickles[:,10]
    #
    dd[:,0,16]=cat[:,8]-cat[:,10] #MB518 - MB604                   
    dd[:,1,16]=cat[:,10]-cat[:,14] #MB604 - MB815     
    ref[:,0,16]=pickles[:,8]-pickles[:,10]                            
    ref[:,1,16]=pickles[:,10]-pickles[:,14]                           
    #
    dd[:,0,17]=cat[:,8]-cat[:,3] #MB518 - R                
    dd[:,1,17]=cat[:,3]-cat[:,16] #R - MB914     
    ref[:,0,17]=pickles[:,8]-pickles[:,3]                            
    ref[:,1,17]=pickles[:,3]-pickles[:,16]                           
    #
    dd[:,0,18]=cat[:,10]-cat[:,14] #MB604 - MB815                      
    dd[:,1,18]=cat[:,14]-cat[:,15] #MB815 - MB856       
    ref[:,0,18]=pickles[:,10]-pickles[:,14]                            
    ref[:,1,18]=pickles[:,14]-pickles[:,15]                           
    #
    dd[:,0,19]=cat[:,10]-cat[:,3] #MB604-R                   
    dd[:,1,19]=cat[:,3]-cat[:,13] #R - MB753   
    ref[:,0,19]=pickles[:,10]-pickles[:,3]                            
    ref[:,1,19]=pickles[:,3]-pickles[:,13]    
    #---------------initialize and run SPM here---------------------- 
    #running SPM on the model to get zero-point offsets
    spa = Softassign.SoftassignProcrustesAnnealing(dd, ref, label='CDFS')
    (dx, dy) = spa.getTranslation()
    print 'dx, dy:', dx, dy
    M = spa.getCorrespondenceMatrix()
    spa.generateCoefficientMatrix(y,new_color,new_color_p,M,ID, 2)
    translations=spa.runCoefficientMatrixSVD()
    print translations
    np.savetxt(os.environ["SPM"]+'/data/zero-points.offsets.CDFS.catalogue.SDSS.Vega.2017.update.asc',translations)
    tcolor=np.loadtxt(os.environ["SPM"]+'/data/SPM.translate.update.asc')
    #plotting
    for i in range(y.shape[0]):
        spm=Softassign.PlotStellarLocusKDEJoin(color[:,i,0],color[:,i,1],new_color[:,i,0],new_color[:,i,1],new_color_p[:,i,0],new_color_p[:,i,1],tcolor[i,0],tcolor[i,1],excluded,ID[y[i,0]],ID[y[i,1]],ID[y[i,1]],ID[y[i,2]],M,plot_label=False)
        spm.plot_colors()

    
