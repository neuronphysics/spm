import os,glob
import warnings
try:
   import pysysp # In order to use this code you need a package for convolving stellar spectra with filters https://pypi.python.org/pypi/pysysp
except Warning:
   warnings.warn("Please, first install pysysp library from https://pypi.python.org/pypi/pysysp .....", ImportWarning)

import pylab as plt
import numpy as np
def get_digits(str1):
    "Find digits in a string"
    c = ""
    for i in str1:
        if i.isdigit():
            c += i
    return c
  
def loadfits(file):
    """Load spectrum"""
    hdulist = pyfits.open(file)
    wavelength = hdulist[1].data.field('Wavelength')
    flux = hdulist[1].data.field('Flux')
    hdulist.close()
    return wavelength, flux

MAINPATH = os.getcwd()
PATH = MAINPATH + "/Pickles/"
FILTER_PATH = MAINPATH + "/filters/"
os.chdir(PATH)
loci =[]
for files in glob.glob("*.fits"):
    #Only first 46 spectra are main sequence stars
    if (int(get_digits(files))< 46):
       loci.append(files)

os.chdir(FILTER_PATH)
wfi=[]
for file in glob.glob("WFI_*.res"):
    wfi.append(file)

filt=[ 'U38', 'B', 'V', 'R', 'I', '420m', '464m', '485m', '518m', '571m', '604m', '646m', '696m', '753m', '815m', '855m', '915m']
stellar_locus=np.zeros((len(loci),len(wfi)),float)
colors=['peru', 'dodgerblue','brown', 'lightsalmon','orange','springgreen','dimgray','fuchsia','navajowhite','pink','yellow','red','darkslateblue','lawngreen', 'black','coral','orchid']
plt.figure(figsize=(15,8))
for i, stars in enumerate(loci):
    StarSpec = pysysp.StarSpectrum(PATH+stars)
    print StarSpec.flux
    for j, f in enumerate(filt): 
        band = pysysp.BandPass(FILTER_PATH+'WFI_'+f+'.res',smt='linear')
        print band.name
        wsmin = np.min(StarSpec.wavelength)
        wsmax = np.max(StarSpec.wavelength)
        wbmin = np.min(band.wavelength)
        wbmax = np.max(band.wavelength)
        x= band.wavelength 
        y= band.response
        if (i<1):
           lines= plt.plot(x,y,label=f )
           plt.setp(lines, color=colors[j], linewidth=1.0)
           plt.xlabel('$\lambda$')
           plt.ylabel('f')
        if wbmin < wsmin:
           StarSpec.wavelength= np.insert(StarSpec.wavelength, 0, band.wavelength[0])
           StarSpec.flux = np.insert(StarSpec.flux, 0, 0)
        if wbmax > wsmax:
           StarSpec.wavelength = np.append(StarSpec.wavelength, band.wavelength[-1])
           StarSpec.flux = np.append(StarSpec.flux,0)
        stellar_locus[i,j] = StarSpec.apmag(band, mag='Vega', mzero=0.0)
        print stars, f
        print stellar_locus[i,j]
        
plt.legend(frameon=False)      
plt.savefig(MAINPATH + '/results/COMBO17_filters.eps',dpi=300)
#The amplitude of flux is different from Pickles catalogue
np.savetxt(MAINPATH + '/StellarLocus/Pickles_standard_stars_main_sequence_vega_system.ascii',stellar_locus)
